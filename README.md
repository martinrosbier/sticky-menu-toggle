# sticky menu toggle

improved from
https://codingreflections.com/hide-header-on-scroll-down/

jQuery + bootstrap

ready to add to any WordPress theme

simplest jQuery script possible to add the following behavior to your bootstrap menu: sticky menu that hides on scroll down and appears in scroll up.

menu should already be sticky.
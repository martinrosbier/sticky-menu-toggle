jQuery(function ($) {

    /* HEADER MENU SCROLL BEHAVIOR */
    var doc = document.documentElement;
    var w = window;
  
    var prevScroll = w.scrollY || doc.scrollTop;
    var curScroll;
    var direction = 0;
    var prevDirection = 0;
  
    var checkScroll = function () {
      /*
       ** Find the direction of scroll
       ** 0 - initial, 1 - up, 2 - down
       */
  
      curScroll = w.scrollY || doc.scrollTop;
      if (curScroll > prevScroll) {
        //scrolled up
        direction = 2;
      } else if (curScroll < prevScroll) {
        //scrolled down
        direction = 1;
      }
  
      if (direction !== prevDirection) {
        toggleHeader(direction, curScroll);
      }
  
      prevScroll = curScroll;
  
    };
  
    var toggleHeader = function (direction, curScroll) {
      var headerHeight = Number(jQuery("#masthead").height());
  
      if (jQuery('.navbar-toggler[aria-expanded="true"]').length > 0) {
        headerHeight += jQuery(".navbar-menues").height();
      }
  
      if (direction !== 0 && direction === 2 && curScroll > headerHeight) {
        jQuery("#masthead").removeClass("scrolled");
        jQuery("#masthead").css("top", -Math.abs(headerHeight));
        prevDirection = direction;
      } else if (direction === 1) {
        jQuery("#masthead").addClass("scrolled");
        jQuery("#masthead").css("top", 0);
        prevDirection = direction;
      }
    };
    
    window.addEventListener("scroll", checkScroll);

  });
  